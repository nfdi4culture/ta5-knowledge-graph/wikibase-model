# RDF Conversion and Wikibase Upload Scripts

This repository contains a collection of Python scripts to convert CSV data to RDF/Turtle format, generate README files for better readability, upload data to Wikibase, and validate the uploaded data.

## Scripts Overview

### 1. `csv2ttl.py`
Converts compatible CSV files to Turtle (TTL) format. The CSV file must have columns matching the expected format, or the script needs to be adjusted accordingly.

### 2. `ttl2readme.py`
Generates a human-readable README file from a Turtle (TTL) file, making it easier to document and share the data structure in the repository.

### 3. `ttl2wb.py`
Uploads a Turtle (TTL) file to a Wikibase instance. The script assumes the TTL file follows the correct format for Wikibase compatibility.

### 4. `validator.py`
Downloads and validates all triples from the Wikibase instance to verify the integrity of the uploaded data.

