import pandas
import pathlib
import rdflib

# Configuration Section
CONFIG = {
    'csv_path': pathlib.Path(r'C:\Arbeitsordner\Generic\wikibase-model\wikibase_generic_model.csv'),  # Path to the input CSV file
    'output_ttl_path': pathlib.Path.cwd() / 'wikibase-generic-model.ttl',         # Path to the output TTL file
    'default_language': 'en',                                   # Default language for literals
    'model': {                                                  
        'name': 'Wikibase Generic Model for Digital Objects',   #title of the model
        'prefix': 'tst',                                        #prefix for the uris in the ttl file
        'authors': ['Lucia Sohmen', 'Lozana Rossenova', 'Paul Duchesne'],
        'version': '2025-02-12',
        'namespace': 'https://gitlab.com/nfdi4culture/wikibase4research/model/'
    }

}

# Function to add URI-based statements to the RDF graph
def assign_statements(sub, col, predicate):
    '''
    Adds URI-based statements to the RDF graph.

    Parameters:
        sub : URI of the subject
        col : Column name in the dataset (x) containing the values
        pre : RDF predicate (e.g., rdfs:subClassOf, owl:sameAs)
        x   : Current dataset record (dictionary of key-value pairs)
    '''
    if col in x and isinstance(x[col], str):
        for elem in x[col].split(','):
            graph.add((sub, predicate, rdflib.URIRef(elem.strip())))

# Function to add literal statements to the RDF graph
def add_literal(sub, col, predicate, lang=None, multiple=False):
    '''
    Add literal statements to the graph.
    Handles both single and multiple values in a column.
    Checks if fields and columns exist
        Parameters:
        sub       : URI of the subject
        col       : Column name in the dataset (x) containing the values
        predicate : RDF predicate (e.g., rdfs:label, skos:altLabel)
        x         : Current dataset record (dictionary of key-value pairs)
        lang      : Language code (e.g., 'en', 'de') for the literals, if applicable
        multiple  : Indicates whether the column contains multiple values (comma-separated)
    '''
    if col in x and isinstance(x[col], str):
        values = x[col].split(',') if multiple else [x[col]]
        for value in values:
            graph.add((sub, predicate, rdflib.Literal(value.strip(), lang=lang)))

# Initialize the RDF graph
graph = rdflib.Graph()

# Define and bind namespaces
prefix = rdflib.Namespace(CONFIG['model']['namespace'])
graph.bind(CONFIG['model']['prefix'], prefix)
graph.bind('skos', rdflib.Namespace('http://www.w3.org/2004/02/skos/core#'))
graph.bind('dc11', rdflib.Namespace('http://purl.org/dc/elements/1.1/'))

#add information about the ontology (authors, version)
model = rdflib.URIRef(CONFIG['model']['namespace'])
graph.add((model, rdflib.RDF.type, rdflib.OWL.Ontology))
graph.add((model, rdflib.URIRef('http://purl.org/dc/elements/1.1/title'), rdflib.Literal(CONFIG['model']['name'])))
for x in CONFIG['model']['authors']:
    graph.add((model, rdflib.URIRef('http://purl.org/dc/elements/1.1/creator'), rdflib.Literal(x)))
graph.add((model, rdflib.OWL.versionInfo, rdflib.Literal(CONFIG['model']['version'])))

# Load the CSV dataset
data = pandas.read_csv(CONFIG['csv_path'])

# Process each record in the dataset
for x in data.to_dict('records'):

    # Create a URI for the subject from the 'iri' column
    iri = prefix[x['iri'].split(':')[1]]

    # Add statements to the subject
    graph.add((iri, rdflib.RDF.type, rdflib.URIRef(x['type'])))
    add_literal(iri, 'label_de', rdflib.RDFS.label, lang='de')
    add_literal(iri, 'label', rdflib.RDFS.label, lang=CONFIG['default_language'])
    add_literal(iri, 'description_de', rdflib.URIRef('http://purl.org/dc/elements/1.1/description'), lang='de')
    add_literal(iri, 'description', rdflib.URIRef('http://purl.org/dc/elements/1.1/description'), lang=CONFIG['default_language'])
    assign_statements(iri, 'instance of', rdflib.RDF.type)
    assign_statements(iri, 'subclass of', rdflib.RDFS.subClassOf)
    assign_statements(iri, 'subproperty of', rdflib.RDFS.subPropertyOf)
    assign_statements(iri, 'domain', rdflib.RDFS.domain)
    assign_statements(iri, 'range', rdflib.RDFS.range)
    assign_statements(iri, 'same as', rdflib.OWL.sameAs)
    assign_statements(iri, 'equivalent class', rdflib.OWL.equivalentClass)
    assign_statements(iri, 'equivalent property', rdflib.OWL.equivalentProperty)
    add_literal(iri, 'alias', rdflib.SKOS.altLabel, lang=CONFIG['default_language'], multiple=True)
    add_literal(iri, 'note', rdflib.SKOS.note, lang=CONFIG['default_language'], multiple=True)

# Serialize the RDF graph into a Turtle file
graph.serialize(destination=CONFIG['output_ttl_path'], format='ttl', encoding='utf-8')

# Print the total number of triples in the graph
print(len(graph), 'triples.')
