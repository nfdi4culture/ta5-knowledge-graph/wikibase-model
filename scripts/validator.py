#Script to download all triples from triplestore for validation/migration purposes.

import pandas
import pathlib
import pydash
import requests

def value_extract(row, col):

    ''' Extract dictionary values. '''
  
    return pydash.get(row[col], "value")
   
def sparql_query(query, service):
 
    ''' Send sparql request, and formulate results into a dataframe. '''

    r = requests.get(service, params={"format": "json", "query": query})
    data = pydash.get(r.json(), "results.bindings")
    data = pandas.DataFrame.from_dict(data)
    for x in data.columns:
        data[x] = data.apply(value_extract, col=x, axis=1)

    return data

query = '''select distinct ?a ?b ?c where {?a ?b ?c}''' 
dataframe = sparql_query(query, " https://query.semantic-kompakkt.de/proxy/wdqs/bigdata/namespace/wdq/sparql")
dataframe.to_parquet(pathlib.Path.cwd() / 'semantic-kompakkt-de.parquet')
print(len(dataframe))
dataframe.head()