import pathlib
import rdflib

CONFIG = {
    'ttl_file': r'filename',  # Input TTL file
    'prefix': 'https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/',  # Prefix for IRIs
    'out_file': 'README.md',  # Output Markdown file
    'model_name' : 'model_name'  # Model name
}

def pull_attribute(sub, tag, prop):
    """
    Pull attributes from the graph against subject entities.
    """
    # Get all values for the given property
    pulled = [(str(o), getattr(o, 'language', None)) for s, p, o in graph.triples((sub, prop, None))]
    
    # If no values are found, return an empty string
    if not len(pulled):
        return ''
    else:
        # Group values by language (default if no language given)
        language_labels = {}
        for text, lang in pulled:
            if lang is not None:
                if lang in language_labels:
                    language_labels[lang].append(text)
                else:
                    language_labels[lang] = [text]
            else:
                if 'default' in language_labels:
                    language_labels['default'].append(text)
                else:
                    language_labels['default'] = [text]
        
        # Format the output string
        if len(language_labels) ==1 and next(iter(language_labels.keys())) == 'default':
            text = ', '.join([x[0] for x in pulled])
            return f"**{tag}** {text}    \n"
        else:
            substring=''    
            for lang, items in language_labels.items():
                text = ', '.join(items)
                substring += f"**{tag}_{lang}** {text}    \n" if lang != 'default' else f"**{tag}** {text}    \n"
            return substring


# Initialize the output string
string = f"## {CONFIG['model_name']}\n"

# Parse the input TTL file
graph = rdflib.Graph()
graph.parse(CONFIG['ttl_file'], format='ttl')

# Add the prefix IRI to the output string
for x in [rdflib.URIRef(CONFIG['prefix'])]:
    string += f"**iri** {x}    \n"
    string += pull_attribute(x, 'authors', rdflib.URIRef('http://purl.org/dc/elements/1.1/creator'))
    string += pull_attribute(x, 'version', rdflib.OWL.versionInfo)

print(len(graph))

# Loop over the different types of entities
for x in [
    {'iri':rdflib.OWL.Class, 'label':'Classes'},
    {'iri':rdflib.OWL.NamedIndividual, 'label':'Individuals'},
    {'iri':rdflib.OWL.ObjectProperty, 'label':'Object Properties'},
    {'iri':rdflib.OWL.DatatypeProperty, 'label':'Datatype Properties'}]:

    # Add a header for the current type of entity
    string += f"## **{x['label']}**    \n"
    
    # Get all entities of the current type
    entities = [s for s,p,o in graph.triples((None, rdflib.RDF.type, x['iri']))]
    
    # Loop over the entities
    for y in sorted(entities):
        # Add the entity IRI to the output string
        string += f"### {pathlib.Path(y).stem}    \n"
        string += f"**iri** {str(y)}    \n"
        
        # Pull attributes for the entity
        string += pull_attribute(y, 'label', rdflib.RDFS.label)        
        string += pull_attribute(y, 'description', rdflib.URIRef('http://purl.org/dc/elements/1.1/description'))
        string += pull_attribute(y, 'alias', rdflib.SKOS.altLabel)
        string += pull_attribute(y, 'type', rdflib.RDF.type)
        string += pull_attribute(y, 'subclass of', rdflib.RDFS.subClassOf)
        string += pull_attribute(y, 'subproperty of', rdflib.RDFS.subPropertyOf)
        string += pull_attribute(y, 'domain', rdflib.RDFS.domain)
        string += pull_attribute(y, 'range', rdflib.RDFS.range)    
        string += pull_attribute(y, 'same as', rdflib.OWL.sameAs)
        string += pull_attribute(y, 'equivalent class', rdflib.OWL.equivalentClass)
        string += pull_attribute(y, 'equivalent property', rdflib.OWL.equivalentProperty)
        string += pull_attribute(y, 'note', rdflib.SKOS.note)

# Write the output string to the Markdown file
with open(pathlib.Path.cwd() / CONFIG['out_file'], 'w', encoding='utf-8') as markdown:
    markdown.write(string)