## Wikibase Generic Model for Digital Objects
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/    
**authors** Lozana Rossenova, Lucia Sohmen, Paul Duchesne    
**version** 2023-04-28    
## **Classes**    
### actor    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**label_de** Akteur:in    
**label_en** Actor    
**description_de** umfasst Menschen, entweder einzeln oder in Gruppen, die das Potenzial haben, vorsätzliche Handlungen zu begehen, für die jemand zur Verantwortung gezogen werden kann    
**description_en** comprises people, either individually or in groups, who have the potential to perform intentional actions of kinds for which someone may be held responsible    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E39_CRM_Actor, http://xmlns.com/foaf/0.1/#term_Agent    
**note** Wikibase ID: Q1    
### annotation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**label_de** Annotation    
**label_en** Annotation    
**description_de** Text, der an ein Medienelement oder einen anderen Text angehängt ist, um weitere Informationen zu liefern    
**description_en** text that is attached to a media item or another text to provide more information    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**equivalent class** http://www.ics.forth.gr/isl/CRMdig/D29_Annotation_Object, http://www.w3.org/ns/oa#Annotation, http://www.wikidata.org/entity/Q857525    
**note** Wikibase ID: Q15    
### bibliographic_work    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/bibliographic_work    
**label_de** Bibliografisches Werk    
**label_en** Bibliographic Work    
**description_de** Instanzen dieser Klasse können zitiert werden, um eine Behauptung zu belegen    
**description_en** instances of this class can be cited to support a claim    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**equivalent class** http://iflastandards.info/ns/fr/frbr/frbroo/F19_Publication_Work, http://www.wikidata.org/entity/Q59156095    
**note** Wikibase ID: Q14    
### biological_object    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/biological_object    
**label_de** Biologischer Gegenstand    
**label_en** Biological Object    
**description_de** umfasst einzelne Gegenstände materieller Natur, die leben, gelebt haben oder natürliche Produkte von oder aus lebenden Organismen sind    
**description_en** comprises individual items of a material nature, which live, have lived or are natural products of or from living organisms    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**equivalent class** http://cidoc-crm.org/cidoc-crm/E20_Biological_Object    
**note** Wikibase ID: Q4    
### birth    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/birth    
**label_de** Geburt    
**label_en** Birth    
**description_de** ein biologisches Ereignis, das sich auf den Kontext konzentriert, in dem Menschen auf die Welt kommen    
**description_en** a biological event focussing on the context of people coming into life    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://cidoc-crm.org/cidoc-crm/7.1.2/E67_Birth    
**note** Wikibase ID: Q71    
### change_of_custody    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/change_of_custody    
**label_de** Besitzwechsel    
**label_en** Change of custody    
**description_de** Ereignis, bei dem der Verwahrer einer Sache wechselt    
**description_en** event where the custodian of an item changes    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E10_Transfer_of_Custody    
**note** Wikibase ID: Q23    
### change_of_ownership    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/change_of_ownership    
**label_de** Eigentumswechsel    
**label_en** Change of ownership    
**description_de** Ereignis, bei dem der Eigentümer einer Sache wechselt    
**description_en** event where the owner of an item changes    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E8_Acquisition    
**note** Wikibase ID: Q24    
### collection    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/collection    
**label_de** Sammlung    
**label_en** Collection    
**description_de** Zusammenstellung von physischen Dingen, die von einem Akteur kuratiert werden – nicht für digitale Objekte, stattdessen »digitale Sammlung« verwenden    
**description_en** aggregation of physical things that are being curated by an actor - not for digital objects, use Digital collection instead    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E78_Collection    
**note** Wikibase ID: Q7    
### conceptual_object    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**label_de** Begrifflicher Gegenstand    
**label_en** Conceptual Object    
**description_de** umfasst nicht materielle Produkte unseres Geistes und andere von Menschen produzierte Daten    
**description_en** comprises non-material products of our minds and other human produced data    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E28_Conceptual_Object    
**note** Wikibase ID: Q8    
### condition    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/condition    
**label_de** Zustand    
**label_en** Condition    
**description_de** Zustand eines Gegenstands    
**description_en** condition of an object    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E3_Condition_State, http://www.wikidata.org/entity/Q813912    
**note** Wikibase ID: Q20    
### country    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/country    
**label_de** Land    
**label_en** Country    
**description_de** bestehendes oder ehemals bestehendes Land    
**description_en** existing or previously existing country    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/place    
**equivalent class** https://nfdi4culture.de/ontology#Country    
**note** Wikibase ID: Q73    
### creation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/creation    
**label_de** Herstellung    
**label_en** Creation    
**description_de** Ereignis, das zur Erstellung eines Objekts führt    
**description_en** event that results in an item being created    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E12_Production    
**note** Wikibase ID: Q25    
### death    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/death    
**label_de** Tod    
**label_en** Death    
**description_de** umfasst den Tod von Menschen    
**description_en** comprises the deaths of human beings    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://cidoc-crm.org/cidoc-crm/7.1.2/E69_Death    
**note** Wikibase ID: Q72    
### destruction    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/destruction    
**label_de** Zerstörung    
**label_en** Destruction    
**description_de** Ereignis, das dazu führt, dass ein Objekt nicht mehr existiert    
**description_en** event that results in an item no longer existing    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E6_Destruction    
**note** Wikibase ID: Q26    
### digital_collection    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/digital_collection    
**label_de** Digitale Sammlung    
**label_en** Digital Collection    
**description_de** Gruppierung digitaler Gegenstände    
**description_en** aggregation of digital objects    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/information_object    
**equivalent class** http://www.wikidata.org/entity/Q60474998    
**note** Wikibase ID: Q10    
### digital_object    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/digital_object    
**label_de** Digitales Objekt    
**label_en** Digital Object    
**description_de** umfasst identifizierbare immaterielle Güter, die als Mengen von Bitfolgen dargestellt werden können, wie Datensätze, elektronische Texte, Bilder, Audio- oder Videoelemente, Software usw., und die als einzelne Einheiten dokumentiert werden    
**description_en** comprises identifiable immaterial items that can be represented as sets of bit sequences, such as data sets, e-texts, images, audio or video items, software, etc., and are documented as single units    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/information_object    
**equivalent class** http://www.ics.forth.gr/isl/CRMdig/D1_Digital_Object, http://www.wikidata.org/entity/Q59138870    
**note** Wikibase ID: Q11    
### digitization    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/digitization    
**label_de** Digitalisierung    
**label_en** Digitization    
**description_de** Ereignis, das zu einer neuen digitalen Objektdarstellung eines Gegenstands führt    
**description_en** event that results in a new digital object representation of an item    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/creation    
**equivalent class** http://www.ics.forth.gr/isl/CRMdig/D2_Digitization_Process    
**note** Wikibase ID: Q27    
### event    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**label_de** Ereignis    
**label_en** Event    
**description_de** Oberklasse für Ereignisse    
**description_en** superclass for events    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E5_Event    
**note** Wikibase ID: Q22    
### file_format    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/file_format    
**label_de** Datenformat    
**label_en** File Format    
**description_de** Kodierungsformat einer digitalen Datei    
**description_en** encoding format of a digital file    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/digital_object    
**equivalent class** http://purl.org/dc/terms/FileFormat    
**note** Wikibase ID: Q39    
### function    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/function    
**label_de** Funktion    
**label_en** Function    
**description_de** beschreibt die Verwendung eines Objekts    
**description_en** describes the use of an item    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**note** Wikibase ID: Q40    
### group    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/group    
**label_de** Menschliche Gruppe    
**label_en** Group    
**description_de** alle Ansammlungen oder Organisationen menschlicher Individuen oder Gruppen, die aufgrund irgendeiner Form von verbindenden Beziehungen kollektiv oder in ähnlicher Weise handeln    
**description_en** any gatherings or organizations of human individuals or groups that act collectively or in a similar way due to any form of unifying relationship    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E74_Group, http://www.wikidata.org/entity/Q16887380, http://xmlns.com/foaf/0.1/#term_Group    
**note** Wikibase ID: Q2    
### human    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human    
**label_de** Mensch    
**label_en** Human    
**description_de** (realer) toter oder lebender Mensch    
**description_en** (real) dead or living human being    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/biological_object    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E21_Person, http://www.wikidata.org/entity/Q5, http://xmlns.com/foaf/0.1/#term_Person, https://schema.org/Person    
**note** Wikibase ID: Q5    
### human-made_object    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**label_de** Menschengemachter Gegenstand    
**label_en** Human-Made Object    
**description_de** alle dauerhaften physischen Gegenstände jeglicher Größe, die absichtlich durch menschliche Aktivitäten geschaffen werden    
**description_en** all persistent physical objects of any size that are purposely created by human activity    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E22_Human-Made_Object, http://www.wikidata.org/entity/Q223557    
**note** Wikibase ID: Q6    
### iconographic_concept    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/iconographic_concept    
**label_de** Ikonografisches Konzept    
**label_en** Iconographic Concept    
**description_de** etwas, das auf einem Gegenstand abgebildet werden kann    
**description_en** something that can be depicted on an object    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**note** Wikibase ID: Q43    
### information_object    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/information_object    
**label_de** Informationsgegenstand    
**label_en** Information Object    
**description_de** umfasst identifizierbare immaterielle Güter wie Datensätze, Bilder, Texte, Multimedia-Objekte, Computercode, Algorithmen oder mathematische Formeln, die eine objektiv erkennbare Struktur aufweisen und als einzelne Einheiten dokumentiert sind    
**description_en** comprises identifiable immaterial items, such as data sets, images, texts, multimedia objects, computer code, algorithm or mathematical formulae, that have an objectively recognizable structure and are documented as single units    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**equivalent class** http://cidoc-crm.org/cidoc-crm/E73_Information_Object    
**note** Wikibase ID: Q9    
### licence    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/licence    
**label_de** Lizenz    
**label_en** Licence    
**description_de** Art der Nutzungsrechte    
**description_en** type of usage right    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/information_object    
**equivalent class** http://www.ebi.ac.uk/swo/SWO_0000002    
**note** Wikibase ID: Q37    
### material    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/material    
**label_de** Material    
**label_en** Material    
**description_de** Material, aus dem ein Gegenstand hergestellt ist    
**description_en** material that an object is made from    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E57_Material, http://www.wikidata.org/entity/Q214609, https://schema.org/material    
**note** Wikibase ID: Q18    
### measurement_unit    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/measurement_unit    
**label_de** Maßeinheit    
**label_en** Measurement Unit    
**description_de** Einheit zur Angabe eines Maßes, z. B. Meter oder Liter    
**description_en** unit for giving a measurement, for example metres or litres    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E58_Measurement_Unit, http://www.wikidata.org/entity/Q47574    
**note** Wikibase ID: Q19    
### media_item    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**label_de** Medienelement    
**label_en** Media item    
**description_de** digitale Darstellung von Medien, etwa Video oder Audio    
**description_en** digital representation of media, for example video or audio    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/digital_object    
**equivalent class** https://schema.org/MediaObject    
**note** Wikibase ID: Q12    
### modification    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/modification    
**label_de** Bearbeitung    
**label_en** Modification    
**description_de** Ereignis, bei dem sich ein Objekt ändert, aber immer noch als dasselbe Objekt betrachtet wird (spezifischere Ereignisse können Teil Hinzufügen und Teil Entfernen sein)    
**description_en** event where an item changes but is still considered the same item (more specific events can be part addition and part removal)    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E11_Modification    
**note** Wikibase ID: Q28    
### motivation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/motivation    
**label_de** Motivation    
**label_en** Motivation    
**description_de** für Anmerkungen zu verwenden    
**description_en** to be used for annotations    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**equivalent class** http://www.w3.org/ns/oa#Motivation, http://www.wikidata.org/entity/Q644302    
**note** Wikibase ID: Q16    
### move    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/move    
**label_de** Ortswechsel    
**label_en** Move    
**description_de** Ereignis, bei dem ein Objekt von einem Ort zu einem anderen bewegt wird    
**description_en** event where an item is moved from one place to another    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E9_Move    
**note** Wikibase ID: Q29    
### organization    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/organization    
**label_de** Organisation    
**label_en** Organization    
**description_de** eine organisierte Gruppe von Menschen, z. B. eine Universität oder eine Forschungsgesellschaft    
**description_en** organized group of humans such as a university or research society    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent class** http://www.wikidata.org/entity/Q43229, http://xmlns.com/foaf/0.1/#term_Organization, https://nfdi4culture.de/ontology#Organization, https://schema.org/Organization    
**note** Wikibase ID: Q35    
### part_addition    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/part_addition    
**label_de** Teilhinzufügung    
**label_en** Part Addition    
**description_de** Ereignis, bei dem ein Teil zu einem Objekt hinzugefügt wird    
**description_en** event where a part gets added to an item    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E79_Part_Addition    
**note** Wikibase ID: Q30    
### part_removal    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/part_removal    
**label_de** Teilentfernung    
**label_en** Part Removal    
**description_de** Ereignis, bei dem ein Teil von einem Objekt entfernt wird    
**description_en** event where a part is removed from an item    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E80_Part_Removal    
**note** Wikibase ID: Q31    
### physical_object    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**label_de** Materieller Gegenstand    
**label_en** Physical Object    
**description_de** umfasst Gegenstände materieller Art, die Dokumentationseinheiten sind und physische Grenzen haben, die sie auf objektive Weise vollständig von anderen Gegenständen trennen    
**description_en** comprises items of a material nature that are units for documentation and have physical boundaries that separate them completely in an objective way from other objects    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://cidoc-crm.org/cidoc-crm/E19_Physical_Object    
**note** Wikibase ID: Q3    
### place    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/place    
**label_de** Ort    
**label_en** Place    
**description_de** jede Art von Standort    
**description_en** any kind of location    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E53_Place, http://www.wikidata.org/entity/Q2221906, https://nfdi4culture.de/ontology#Place, https://schema.org/Place    
**note** Wikibase ID: Q21    
### position    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/position    
**label_de** Position    
**label_en** Position    
**description_de** Position eines Objekts innerhalb eines Standorts (etwa an der Wand oder Decke)    
**description_en** position of an item within a location (for example wall or ceiling)    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**note** Wikibase ID: Q41    
### primitive_value    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/primitive_value    
**label_de** Primitiver Wert    
**label_en** Primitive Value    
**description_de** umfasst Werte von primitiven Datentypen von Programmiersprachen oder Datenbankverwaltungssystemen und aus solchen Werten zusammengesetzte Datentypen, die als Dokumentationselemente verwendet werden, sowie deren mathematische Abstraktionen    
**description_en** comprises values of primitive data types of programming languages or database management systems and data types composed of such values used as documentation elements, as well as their mathematical abstractions    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://cidoc-crm.org/cidoc-crm/7.1.2/E59_Primitive_Value    
**note** Wikibase ID: Q34    
### raw_data_creation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/raw_data_creation    
**label_de** Rohdatenerzeugung    
**label_en** Raw data creation    
**description_de** Ereignis, das zur Erstellung der Rohdaten eines Objekts führt    
**description_en** event that results in the raw data for an item being created    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/creation    
**note** Wikibase ID: Q32    
### sex_or_gender    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/sex_or_gender    
**label_de** Geschlecht oder Geschlechtsidentität    
**label_en** Sex or gender    
**description_de** für Menschen zu verwenden    
**description_en** to be used for humans    
**type** http://www.w3.org/2002/07/owl#Class    
**equivalent class** http://purl.obolibrary.org/obo/NCIT_C28421, http://www.wikidata.org/entity/Q18382802    
**note** Wikibase ID: Q36    
### software    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/software    
**label_de** Software    
**label_en** Software    
**description_de** für die Erstellung von Medienelementen verwendete Software    
**description_en** software used for creation of media item    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/digital_object    
**equivalent class** http://www.ics.forth.gr/isl/CRMdig/D14_Software, http://www.wikidata.org/entity/Q7397, https://schema.org/SoftwareApplication    
**note** Wikibase ID: Q13    
### style    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/style    
**label_de** Stil    
**label_en** Style    
**description_de** Stil eines Kulturguts    
**description_en** style of a cultural heritage object
    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**note** Wikibase ID: Q42    
### technique    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/technique    
**label_de** Verfahren    
**label_en** Technique    
**description_de** Technik oder Methode, die zur Erstellung oder Änderung eines Gegenstands verwendet wird    
**description_en** technique or method that is used for creating or changing an object     
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**equivalent class** http://www.wikidata.org/entity/Q2695280    
**note** Wikibase ID: Q38    
### transformation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/transformation    
**label_de** Umwandlung    
**label_en** Transformation    
**description_de** Ereignis, bei dem ein Objekt so stark verändert wird, dass es nicht mehr als dasselbe Objekt angesehen werden kann - das Ergebnis ist ein neues Objekt    
**description_en** event where one item is changed so significantly it can no longer be considered the same item - the result is a new item    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E81_Transformation    
**note** Wikibase ID: Q33    
### type    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**label_de** Typus    
**label_en** Type    
**description_de** umfasst Konzepte, die durch Begriffe aus Thesauri und kontrollierten Vokabularien bezeichnet werden, die zur Charakterisierung und Klassifizierung von Instanzen der Kernklassen der Ontologie verwendet werden    
**description_en** comprises concepts denoted by terms from thesauri and controlled vocabularies used to characterize and classify instances of core ontology classes    
**type** http://www.w3.org/2002/07/owl#Class    
**subclass of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**equivalent class** http://www.cidoc-crm.org/cidoc-crm/E55_Type    
**note** Wikibase ID: Q17    
## **Individuals**    
### all_rights    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/all_rights    
**label_de** Alle Rechte vorbehalten    
**label_en** All Rights Reserved    
**description_de** Rechtserklärung zu Werken, die dem Urheberrecht unterliegen    
**description_en** rights statement pertaining to works which are in copyright    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q1752207    
**note** Wikibase ID: Q54    
### assessing    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/assessing    
**label_de** beurteilen    
**label_en** assessing    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, die Zielentität zu bewerten, anstatt nur eine Bemerkung über sie zu machen    
**description_en** used with P80 (motivation) for when the user intends to assess the target entity in some way, rather than simply make a comment about it    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#assessing    
**note** Wikibase ID: Q58    
### bookmarking    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/bookmarking    
**label_de** Lesezeichen setzen    
**label_en** bookmarking    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, ein Lesezeichen für die Zielentität oder einen Teil davon zu setzen    
**description_en** used with P80 (motivation) for when the user intends to create a bookmark to the target entity or part thereof    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#bookmarking    
**note** Wikibase ID: Q59    
### cc-by    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/cc-by    
**label_de** Creative Commons Namensnennung    
**label_en** Creative Commons Attribution    
**description_de** Lizenz, die die freie Nutzung Copyright geschützten Materials unter Nennung des Namens erlaubt    
**description_en** license allowing free use of a copyrighted work, with attribution    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q6905323    
**note** Wikibase ID: Q47    
### cc-by-nc    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/cc-by-nc    
**label_de** Creative Commons Namensnennung-NichtKommerziell    
**label_en** Creative Commons Attribution-NonCommercial    
**description_de** Lizenzsatz, der die nicht kommerzielle Verbreitung erlaubt und die Nennung des Namens erfordert    
**description_en** set of licenses allowing free noncommercial use, and requiring attribution    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q6936496    
**note** Wikibase ID: Q49    
### cc-by-nc-nd    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/cc-by-nc-nd    
**label_de** Creative Commons Namensnennung-NichtKommerziell-KeineBearbeitung    
**label_en** Creative Commons Attribution-NonCommercial-NoDerivatives    
**description_de** Lizenzsatz, der die nicht kommerzielle Verbreitung ohne Änderungen erlaubt und die Nennung des Namens erfordert    
**description_en** set of licenses allowing noncommercial distribution without modification, and requiring attribution    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q6937225    
**note** Wikibase ID: Q50    
### cc-by-nc-sa    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/cc-by-nc-sa    
**label_de** Creative Commons Namensnennung-NichtKommerziell-WeitergabeUnterGleichenBedingungen    
**label_en** Creative Commons Attribution-NonCommercial-ShareAlike    
**description_de** Lizenz, die die nicht kommerzielle Nutzung und Verbreitung unter derselben Lizenz und der Nennung des Namens erlaubt    
**description_en** license allowing noncommercial use and distribution, with attribution, under the same license    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q6998997    
**note** Wikibase ID: Q52    
### cc-by-nd    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/cc-by-nd    
**label_de** Creative Commons Namensnennung-KeineBearbeitung    
**label_en** Creative Commons Attribution-NoDerivatives    
**description_de** Lizenzsatz, der die Verbreitung ohne Änderungen erlaubt und die Nennung des Namens erfordert    
**description_en** set of licenses allowing distribution without modification, and requiring attribution    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q6999319    
**note** Wikibase ID: Q51    
### cc-by-sa    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/cc-by-sa    
**label_de** Creative Commons Namensnennung-WeitergabeUnterGleichenBedingungen    
**label_en** Creative Commons Attribution-ShareAlike    
**description_de** Lizenzsatz, der die Nutzung unter derselben Lizenz erlaubt und die Nennung des Namens erfordert    
**description_en** set of licenses allowing free use of a work, with attribution, under the same conditions    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q6905942    
**note** Wikibase ID: Q48    
### cc0    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/cc0    
**label_de** CC0    
**label_en** CC0    
**description_de** Rechtsdokument, mit dem ein urheberrechtlich geschütztes Werk der Öffentlichkeit zugänglich gemacht wird    
**description_en** legal document dedicating a copyrighted work to the public domain    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q6938433    
**note** Wikibase ID: Q46    
### classifying    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/classifying    
**label_de** klassifizieren    
**label_en** classifying    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, die Zielentität als etwas zu klassifizieren    
**description_en** used with P80 (motivation) for when the user intends to classify the target entity as something    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#classifying    
**note** Wikibase ID: Q60    
### commenting    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/commenting    
**label_de** kommentieren    
**label_en** commenting    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, einen Kommentar über die Zielentität abzugeben    
**description_en** used with P80 (motivation) for when the user intends to comment about the target entity    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#commenting    
**note** Wikibase ID: Q61    
### describing    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/describing    
**label_de** beschreiben    
**label_en** describing    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, die Zielentität zu beschreiben    
**description_en** used with P80 (motivation) for when the user intends to describe the target entity    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#describing    
**note** Wikibase ID: Q62    
### editing    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/editing    
**label_de** bearbeiten    
**label_en** editing    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, eine Änderung oder Bearbeitung der Zielentität zu beantragen    
**description_en** used with P80 (motivation) for when the user intends to request a change or edit to the target entity    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#editing    
**note** Wikibase ID: Q63    
### false    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/false    
**label_de** FALSE    
**label_en** FALSE    
**description_de** Wert für bestimmte Eigenschaften, wie P83 (verifiziert)    
**description_en** value for specific properties, like P83 (verified)
    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** https://schema.org/False    
**note** Wikibase ID: Q45    
### female    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/female    
**label_de** weiblich    
**label_en** female    
**description_de** gibt an, dass es sich bei der Person um eine Frau handelt    
**description_en** indicates that the human subject is a female    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** https://www.wikidata.org/wiki/Q6581072    
**note** Wikibase ID: Q55    
### highlighting    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/highlighting    
**label_de** hervorheben    
**label_en** highlighting    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, die Zielentität oder ein Segment davon zu markieren    
**description_en** used with P80 (motivation) for when the user intends to highlight the target entity or segment of it    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#highlighting    
**note** Wikibase ID: Q64    
### identifying    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/identifying    
**label_de** identifizieren    
**label_en** identifying    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, der Zielentität eine Identität zuzuweisen    
**description_en** used with P80 (motivation) for when the user intends to assign an identity to the target entity    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#identifying    
**note** Wikibase ID: Q65    
### linking    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/linking    
**label_de** verlinken    
**label_en** linking    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, einen Link zu einer Ressource zu erstellen, die mit der Zielentität in Verbindung steht    
**description_en** used with P80 (motivation) for when the user intends to link to a resource related to the target entity    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#linking    
**note** Wikibase ID: Q66    
### male    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/male    
**label_de** männlich    
**label_en** male    
**description_de** gibt an, dass es sich bei der Person um einen Mann handelt    
**description_en** indicates that the human subject is a male    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** https://www.wikidata.org/wiki/Q6581097    
**note** Wikibase ID: Q56    
### moderating    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/moderating    
**label_de** moderieren    
**label_en** moderating    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, der Zielentität einen Wert oder eine Qualität zuzuweisen    
**description_en** used with P80 (motivation) for when the user intends to assign some value or quality to the target entity    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#moderating    
**note** Wikibase ID: Q67    
### non-binary    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/non-binary    
**label_de** nicht-binär    
**label_en** non-binary    
**description_de** andere Geschlechtsidentitäten als männlich oder weiblich    
**description_en** gender identities other than male or female
    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** https://www.wikidata.org/wiki/Q48270    
**note** Wikibase ID: Q57    
### questioning    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/questioning    
**label_de** hinterfragen    
**label_en** questioning    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, eine Frage über die Zielentität zu stellen    
**description_en** used with P80 (motivation) for when the user intends to ask a question about the target entity    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#questioning    
**note** Wikibase ID: Q68    
### replying    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/replying    
**label_de** beantworten    
**label_en** replying    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, auf eine frühere Aussage zu antworten, entweder auf eine Anmerkung oder eine andere Ressource    
**description_en** used with P80 (motivation) for when the user intends to reply to a previous statement, either an annotation or another resource    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#replying    
**note** Wikibase ID: Q69    
### some_rights    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/some_rights    
**label_de** einige Rechte vorbehalten    
**label_en** Some rights reserved    
**description_de** Lizenz, die eine eingeschränkte Nutzung erlaubt    
**description_en** license that allows limited use    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.wikidata.org/entity/Q47530729    
**note** Wikibase ID: Q53    
### tagging    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/tagging    
**label_de** markieren    
**label_en** tagging    
**description_de** wird zusammen mit P80 (Motivation) verwendet, wenn die/der Benutzende beabsichtigt, die Zielentität    
**description_en** used with P80 (motivation) for when the user intends to associate a tag with the target entity    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** http://www.w3.org/ns/oa#tagging    
**note** Wikibase ID: Q70    
### true    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/true    
**label_de** TRUE    
**label_en** TRUE    
**description_de** Wert für bestimmte Eigenschaften, wie P83 (verifiziert)    
**description_en** value for specific properties, like P83 (verified)
    
**type** http://www.w3.org/2002/07/owl#NamedIndividual    
**same as** https://schema.org/True    
**note** Wikibase ID: Q44    
## **Object Properties**    
### affiliation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/affiliation    
**label_de** Zugehörigkeit    
**label_en** affiliation    
**description_de** Zugehörigkeit einer Person oder Organisation zu einem Gegenstand    
**description_en** affiliation of a person or organisation to an item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P107_has_current_or_former_member, http://www.wikidata.org/entity/P1416, https://schema.org/affiliation    
**note** Wikibase ID: P50, Wikibase datatype: wikibase-item    
### annotates    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotates    
**label_de** ist Annotation von    
**label_en** annotates    
**description_de** identifiziert die Entität, die das Ziel einer Anmerkung ist    
**description_en** identifies the entity that is the target of an annotation    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**equivalent property** http://www.ics.forth.gr/isl/CRMdig/L43_annotates    
**note** Wikibase ID: P77, Wikibase datatype: wikibase-item    
### by_mother    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/by_mother    
**label_de** durch Mutter    
**label_en** by mother    
**description_de** verknüpft das Ereignis Geburt mit einer Instanz eines Akteurs in der Rolle der gebärenden Mutter    
**description_en** links the event Birth to an instance of an actor in the role of birth-giving mother    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://cidoc-crm.org/cidoc-crm/7.1.2/P96_by_mother    
**note** Wikibase ID: P40, Wikibase datatype: wikibase-item    
### carried_out_by    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/carried_out_by    
**label_de** wurde ausgeführt von    
**label_en** carried out by    
**description_de** aktiver Akteur eines Ereignisses    
**description_en** active agent of an event    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by, https://schema.org/agent    
**note** Wikibase ID: P14, Wikibase datatype: wikibase-item    
### classified_as    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/classified_as    
**label_de** klassifiziert als    
**label_en** classified as    
**description_de** ermöglicht die Subtypisierung von Kernentitäten der Ontologie – eine Form der Spezialisierung – durch die Verwendung einer terminologischen Hierarchie oder eines Thesaurus    
**description_en** allows sub-typing of core ontology entities – a form of specialisation – through the use of a terminological hierarchy, or thesaurus    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/type    
**equivalent property** https://cidoc-crm.org/html/cidoc_crm_v7.1.2.html#P2    
**note** Wikibase ID: P97, Wikibase datatype: wikibase-item    
### consists_of    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/consists_of    
**label_de** besteht aus    
**label_en** consists of    
**description_de** Material, aus dem ein Objekt hergestellt ist    
**description_en** material an item is made from    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/pysical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/material    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P45_consists_of, http://www.wikidata.org/entity/P186, https://schema.org/material    
**note** Wikibase ID: P23, Wikibase datatype: wikibase-item    
### contact_person    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/contact_person    
**label_de** Kontaktperson    
**label_en** contact person    
**description_de** Akteur, welcher die offizielle Anlaufstelle für ein Objekt ist    
**description_en** agent who is the official contact point about an item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/pysical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**note** Wikibase ID: P51, Wikibase datatype: wikibase-item    
### curator    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/curator    
**label_de** Kurator    
**label_en** curator    
**description_de** Akteur, welcher eine Sammlung kuratiert    
**description_en** agent who curates a collection    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/collection    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P109_has_current_or_former_curator, http://www.wikidata.org/entity/P1640    
**note** Wikibase ID: P46, Wikibase datatype: wikibase-item    
### custody_received_by    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/custody_received_by    
**label_de** Gewahrsam übertragen auf    
**label_en** custody received by    
**description_de** neuer Verwahrer dieses Objekts    
**description_en** new custodian of this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P29_custody_received_by    
**note** Wikibase ID: P20, Wikibase datatype: wikibase-item    
### custody_surrendered_by    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/custody_surrendered_by    
**label_de** Gewahrsam übergeben von    
**label_en** custody surrendered by    
**description_de** vorheriger Verwahrer dieses Objekts    
**description_en** previous custodian of this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/group, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P28_custody_surrendered_by    
**note** Wikibase ID: P19, Wikibase datatype: wikibase-item    
### depicts    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/depicts    
**label_de** bildet ab    
**label_en** depicts    
**description_de** Konzept, das auf einem Objekt abgebildet ist    
**description_en** concept that is depicted on an item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/pysical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P62_depicts, http://www.wikidata.org/entity/P180    
**note** Wikibase ID: P64, Wikibase datatype: wikibase-item    
### from_father    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/from_father    
**label_de** gab Vaterschaft    
**label_en** from father    
**description_de** verknüpft das Ereignis Geburt mit einer Instanz eines Akteurs in der Rolle des biologischen Vaters    
**description_en** links the event Birth to an instance of an actor in the role of biological father    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://cidoc-crm.org/cidoc-crm/7.1.2/P97_from_father    
**note** Wikibase ID: P41, Wikibase datatype: wikibase-item    
### had_participant    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/had_participant    
**label_de** hatte Teilnehmer    
**label_en** had participant    
**description_de** Person, die an einem Ereignis teilgenommen hat    
**description_en** person who participated in an event    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P11_had_participant, http://www.wikidata.org/entity/P710, https://schema.org/participant    
**note** Wikibase ID: P13, Wikibase datatype: wikibase-item    
### has_annotation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_annotation    
**label_de** hat Annotation    
**label_en** has annotation    
**description_de** Anmerkung, die für dieses Medienelement gemacht wurde    
**description_en** annotation that was done for this media item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**equivalent property** http://www.ics.forth.gr/isl/CRMdig/L43_annotates    
**note** Wikibase ID: P75, Wikibase datatype: wikibase-item    
### has_condition    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_condition    
**label_de** hat Zustand    
**label_en** has condition    
**description_de** Zustand eines Gegenstands    
**description_en** condition of an object    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/pysical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/condition    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P44_has_condition    
**note** Wikibase ID: P26, Wikibase datatype: wikibase-item    
### has_event    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_event    
**label_de** hat Ereignis    
**label_en** has event    
**description_de** etwas, das mit diesem Objekt passiert ist    
**description_en** something that happened to this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**note** Wikibase ID: P8, Wikibase datatype: wikibase-item    
### has_file_format    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_file_format    
**label_de** hat Datenformat    
**label_en** has file format    
**description_de** Format der Datei    
**description_en** format of the file    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/file_format    
**equivalent property** http://www.wikidata.org/entity/P2701, https://schema.org/encodingFormat    
**note** Wikibase ID: P70, Wikibase datatype: wikibase-item    
### has_motivation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_motivation    
**label_de** hat Motivation    
**label_en** has motivation    
**description_de** eine Eigenschaft aus dem W3C-Anmerkungsstandard, die die Gründe für die Erstellung einer Anmerkung angibt    
**description_en** a property from the W3C annotation standard providing the reasons why an annotation was made    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/motivation    
**equivalent property** http://www.w3.org/ns/oa#motivatedBy    
**note** Wikibase ID: P80, Wikibase datatype: wikibase-item    
### has_part    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_part    
**label_de** hat Teil    
**label_en** has part    
**description_de** ein anderes Objekt, das Teil dieses Objekts ist    
**description_en** another item that is part of this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P45_consists_of, http://www.wikidata.org/entity/P527, https://schema.org/hasPart    
**note** Wikibase ID: P5, Wikibase datatype: wikibase-item    
### has_position    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_position    
**label_de** hat Position    
**label_en** has position    
**description_de** gibt die Position eines Objekts innerhalb eines Standorts an    
**description_en** indicates the position of an item within a location    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/pysical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/position    
**note** Wikibase ID: P39, Wikibase datatype: wikibase-item    
### has_representation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_representation    
**label_de** hat Darstellung    
**label_en** has representation    
**description_de** Medienelement, das dieses Objekt darstellt    
**description_en** media item that represents this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/pysical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P138_represents    
**note** Wikibase ID: P62, Wikibase datatype: wikibase-item    
### has_sex_or_gender    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_sex_or_gender    
**label_de** hat Geschlecht    
**label_en** has sex or gender    
**description_de** Geschlecht oder Geschlechtsidentität einer Person    
**description_en** sex or gender identity of a person    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/sex_or_gender    
**equivalent property** http://www.wikidata.org/entity/P21    
**note** Wikibase ID: P49, Wikibase datatype: wikibase-item    
### has_software    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_software    
**label_de** hat Software    
**label_en** has software    
**description_de** die für die Erstellung des Medienelements verwendete Software    
**description_en** software used for creation of media item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/software    
**note** Wikibase ID: P66, Wikibase datatype: wikibase-item    
### has_technique    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_technique    
**label_de** hat Verfahren    
**label_en** has technique    
**description_de** Technik oder Methode, die zur Erstellung oder Änderung eines Gegenstands verwendet wird    
**description_en** technique or method that is used for creating or changing an object    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/technique    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P32_used_general_technique    
**note** Wikibase ID: P65, Wikibase datatype: wikibase-item    
### in_custody_of    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/in_custody_of    
**label_de** in Gewahrsam von    
**label_en** in custody of    
**description_de** Agent, welcher eine Objekt verwahrt    
**description_en** agent who holds an item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P49_has_former_or_current_keeper    
**note** Wikibase ID: P43, Wikibase datatype: wikibase-item    
### instance_of    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/instance_of    
**label_de** ist ein(e)    
**label_en** instance of    
**description_de** dieses Objekt ist ein spezifisches Beispiel und gehört zu dieser Klasse    
**description_en** this item is a specific example and a member of that class    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**equivalent property** http://www.wikidata.org/entity/P31    
**note** Wikibase ID: P1, Wikibase datatype: wikibase-item    
### is_documented_in    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/is_documented_in    
**label_de** wird belegt in    
**label_en** is documented in    
**description_de** verweist auf einen bibliografischen Titel, der diesen Titel dokumentiert oder weitere Informationen über ihn liefert    
**description_en** points to a bibliographic item which documents this item or provides further information about it    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/pysical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/bibliographic_work    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P70_documents    
**note** Wikibase ID: P58, Wikibase datatype: wikibase-item    
### is_parent_of    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/is_parent_of    
**label_de** ist Elternteil von    
**label_en** is parent of    
**description_de** assoziiert eine Instanz von Mensch mit einer anderen Instanz von Mensch, die die Rolle des Kindes der ersten Instanz spielt    
**description_en** associates an instance of Human with another instance of Human who plays the role of the first instances child    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human    
**equivalent property** http://cidoc-crm.org/cidoc-crm/7.1.2/P152_has_parent    
**note** Wikibase ID: P48, Wikibase datatype: wikibase-item    
### is_part_of    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/is_part_of    
**label_de** ist Teil von    
**label_en** is part of    
**description_de** das Objekt, zu dem dieses Objekt gehört    
**description_en** the item that this item is a part of    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P46_is_composed_of, http://www.wikidata.org/entity/P361, https://schema.org/isPartOf    
**note** Wikibase ID: P6, Wikibase datatype: wikibase-item    
### is_sibling_of    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/is_sibling_of    
**label_de** ist Geschwisterteil von    
**label_en** is sibling of    
**description_de** assoziiert eine Instanz von Mensch mit einer anderen Instanz von Mensch, die die Rolle des Geschwisterteils der ersten Instanz spielt    
**description_en** associates an instance of Human with another instance of Human who plays the role of the first instances sibling    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human    
**note** Wikibase ID: P47, Wikibase datatype: wikibase-item    
### license    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/license    
**label_de** Lizenz    
**label_en** license    
**description_de** die Lizenz, die die Nutzungsrechte für diesen Artikel klärt (verwenden Sie gegebenenfalls auch die Eigenschaft »zitieren als«, um die richtige Zitierung anzugeben)    
**description_en** the license that clarifies use rights for this item (also use property 'cite as' to indicate the proper citation if applicable)    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/license    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P104_is_subject_to, http://www.wikidata.org/entity/P275, https://schema.org/license    
**note** Wikibase ID: P53, Wikibase datatype: wikibase-item    
### location    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/location    
**label_de** Standort    
**label_en** location    
**description_de** beschreibt, wo sich ein Objekt befindet    
**description_en** describes where an item is located    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/place    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P53_has_former_or_current_location, http://www.wikidata.org/entity/P276, https://schema.org/location    
**note** Wikibase ID: P38, Wikibase datatype: wikibase-item    
### material_used    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/material_used    
**label_de** verwendetes Material    
**label_en** material used    
**description_de** Material, das zur Erstellung oder Modifikation eines Objekts verwendet wird    
**description_en** material used for creating or changing an item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/material    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P126_employed    
**note** Wikibase ID: P22, Wikibase datatype: wikibase-item    
### moved_from    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/moved_from    
**label_de** Ortswechsel von    
**label_en** moved from    
**description_de** früherer Standort dieses Objekts    
**description_en** previous location of this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/place    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P27_moved_from    
**note** Wikibase ID: P18, Wikibase datatype: wikibase-item    
### moved_to    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/moved_to    
**label_de** Ortswechsel nach    
**label_en** moved to    
**description_de** neuer Standort dieses Objekts    
**description_en** new location of this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/place    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P26_moved_to    
**note** Wikibase ID: P17, Wikibase datatype: wikibase-item    
### object_of_representation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/object_of_representation    
**label_de** Gegenstand der Darstellung    
**label_en** object of representation    
**description_de** das Objekt, das durch dieses Medienelement dargestellt wird    
**description_en** the item that is represented by this media item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/pysical_object    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P138_represents    
**note** Wikibase ID: P63, Wikibase datatype: wikibase-item    
### owner    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/owner    
**label_de** Eigentümer    
**label_en** owner    
**description_de** Agent, welcher ein Objekt besitzt    
**description_en** agent who owns an item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner, http://www.wikidata.org/entity/P127    
**note** Wikibase ID: P44, Wikibase datatype: wikibase-item    
### part_added    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/part_added    
**label_de** Teil hinzugefügt    
**label_en** part added    
**description_de** welcher Teil zu diesem Objekt hinzugefügt wurde    
**description_en** which part was added to this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P111_added    
**note** Wikibase ID: P28, Wikibase datatype: wikibase-item    
### part_removed    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/part_removed    
**label_de** Teil entfernt    
**label_en** part removed    
**description_de** welcher Teil von diesem Objekt entfernt wurde    
**description_en** which part was removed from this item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P113_removed    
**note** Wikibase ID: P27, Wikibase datatype: wikibase-item    
### placeholder    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/placeholder    
**label_de** Platzhalter    
**label_en** placeholder    
**description_de** Platzhalterobjekt    
**description_en** placeholder property    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**note** Wikibase ID: P21, Wikibase datatype: wikibase-item    
### related_concept    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/related_concept    
**label_de** verwandter Begriff    
**label_en** related concept    
**description_de** verwenden Sie diese Eigenschaft, um den Anmerkungen Elementbeziehungen hinzuzufügen    
**description_en** use this property to add item relations to annotations    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object    
**note** Wikibase ID: P81, Wikibase datatype: wikibase-item    
### related_media    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/related_media    
**label_de** zugehörige Medien    
**label_en** related media    
**description_de** verwenden Sie diese Eigenschaft, um Medienbeziehungen zu Anmerkungen hinzuzufügen    
**description_en** use this property to add media relations to annotations    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**note** Wikibase ID: P82, Wikibase datatype: wikibase-item    
### resulted_in    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/resulted_in    
**label_de** ergab    
**label_en** resulted in    
**description_de** neues Objekt, das durch eine Transformation entstanden ist    
**description_en** new item created by a transformation    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P123_resulted_in    
**note** Wikibase ID: P29, Wikibase datatype: wikibase-item    
### right_held_by    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/right_held_by    
**label_de** Rechte gehalten von    
**label_en** right held by    
**description_de** identifiziert den Akteur, der die Rechte an einem Objekt besitzt    
**description_en** identifies the agent who owns the rights to an item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://cidoc-crm.org/cidoc-crm/7.1.2/P105_right_held_by    
**note** Wikibase ID: P45, Wikibase datatype: wikibase-item    
### spouse    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/spouse    
**label_de** Ehepartner    
**label_en** spouse    
**description_de** das Subjekt hat das Objekt als seinen/ihren Ehepartner (Ehemann, Ehefrau, Partner usw.)    
**description_en** the subject has the object as their spouse (husband, wife, partner, etc.)    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.wikidata.org/entity/P26    
**note** Wikibase ID: P42, Wikibase datatype: wikibase-item    
### stated_in    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/stated_in    
**label_de** angegeben in    
**label_en** stated in    
**description_de** verweist auf den bibliografischen Eintrag, in dem eine Behauptung angegeben wurde – zu verwenden im Feld Referenzen unter einer Behauptung    
**description_en** points to the bibliographic item where a claim was stated in - to be used in the references field under a statement    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/bibliographic_work    
**equivalent property** http://www.wikidata.org/entity/P248    
**note** Wikibase ID: P55, Wikibase datatype: wikibase-item    
### subclass_of    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/subclass_of    
**label_de** Unterklasse von    
**label_en** subclass of    
**description_de** verweist auf die Oberklasse dieser Klasse    
**description_en** points to the superclass of this class    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**equivalent property** http://www.wikidata.org/entity/P279    
**note** Wikibase ID: P2, Wikibase datatype: wikibase-item    
### subproperty_of    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/subproperty_of    
**label_de** Untereigenschaft von    
**label_en** subproperty of    
**description_de** verweist auf die übergeordnete Eigenschaft dieser Eigenschaft    
**description_en** points to the superproperty of this property    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**equivalent property** http://www.wikidata.org/entity/P1647    
**note** Wikibase ID: P3, Wikibase datatype: wikibase-property    
### took_place_at    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/took_place_at    
**label_de** fand statt in    
**label_en** took place at    
**description_de** Ort eines Ereignisses    
**description_en** location of an event    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/place    
**note** Wikibase ID: P9, Wikibase datatype: wikibase-item    
### transferred_title_from    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/transferred_title_from    
**label_de** Eigentum übertragen von    
**label_en** transferred title from    
**description_de** bei Eigentümerwechsel: neuer Eigentümer    
**description_en** for ownership changes: new owner    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P23_transferred_title_from    
**note** Wikibase ID: P16, Wikibase datatype: wikibase-item    
### transferred_title_to    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/transferred_title_to    
**label_de** Eigentum übertragen auf    
**label_en** transferred title to    
**description_de** bei Besitztitelwechsel: vorheriger Besitzer    
**description_en** for ownership changes: previous owner    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P22_transferred_title_to    
**note** Wikibase ID: P15, Wikibase datatype: wikibase-item    
### used_for    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/used_for    
**label_de** benutzt für    
**label_en** used for    
**description_de** Zweck oder Verwendung eines Objekts    
**description_en** purpose or use of an item    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/function    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P101_had_as_general_use, http://www.wikidata.org/entity/P366    
**note** Wikibase ID: P30, Wikibase datatype: wikibase-item    
### verified    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/verified    
**label_de** verifiziert    
**label_en** verified    
**description_de** Status einer Anmerkung vor oder nach der Überprüfung durch einen Benutzer mit höheren Bearbeitungsrechten (normalerweise true oder false)    
**description_en** the status of an annotation before or after review by a user with higher editing privileges (usually true or false)    
**type** http://www.w3.org/2002/07/owl#ObjectProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#boolean    
**note** Wikibase ID: P83, Wikibase datatype: wikibase-item    
## **Datatype Properties**    
### address    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/address    
**label_de** Adresse    
**label_en** address    
**description_de** Adresse eines Objektes, in der Regel eines Gebäudes oder einer Organisation    
**description_en** street address of an item, usually a building or organisation    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P6375, https://schema.org/address    
**note** Wikibase ID: P36, Wikibase datatype: string    
### annotation_text    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation_text    
**label_de** Anmerkungstext    
**label_en** annotation text    
**description_de** Link zur URL, die den vollständigen Text enthält    
**description_en** link to the URL that has the full text    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#string    
**note** Wikibase ID: P76, Wikibase datatype: url    
### bears_feature    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/bears_feature    
**label_de** trägt Merkmal    
**label_en** bears feature    
**description_de** Merkmal eines Gegenstand    
**description_en** feature of an object    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P56_bears_feature    
**note** Wikibase ID: P24, Wikibase datatype: string    
### camera_type    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/camera_type    
**label_de** Kameratyp    
**label_en** camera type    
**description_de** Kompakkt-spezifische Eigenschaft, die sich auf die Perspektive der Kamera bei der Beschriftung eines 3D-Modells bezieht    
**description_en** Kompakkt-specific property relating to the perspective of the camera when annotating a 3D model    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#string    
**note** Wikibase ID: P78, Wikibase datatype: string    
### conforms_to    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conforms_to    
**label_de** stimmt überein mit    
**label_en** conforms to    
**description_de** Norm, welcher die Datei entspricht    
**description_en** which standard the file conforms to    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/digital_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**note** Wikibase ID: P72, Wikibase datatype: string    
### contact_point    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/contact_point    
**label_de** Kontaktpunkt    
**label_en** contact point    
**description_de** wie eine Organisation oder Gruppe kontaktiert werden kann    
**description_en** how an organisation or group can be contacted    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/group    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P76_has_contact_point    
**note** Wikibase ID: P52, Wikibase datatype: string    
### coordinate_location    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/coordinate_location    
**label_de** geographische Koordinaten    
**label_en** coordinate location    
**description_de** genauer Standort eines Objekts    
**description_en** exact location of an item    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/place    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P625, https://schema.org/geo    
**note** Wikibase ID: P37, Wikibase datatype: globe-coordinate    
### copyright_statement    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/copyright_statement    
**label_de** Urheberrechtserklärung    
**label_en** copyright statement    
**description_de** wie Sie den Artikel in Übereinstimmung mit der Lizenz zitieren (verwenden Sie die Eigenschaft „Lizenz“, um die Nutzungsrechte anzugeben)    
**description_en** how to cite this item in compliance with the license (also use property 'license' to indicate usage rights)    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P105_right_held_by    
**note** Wikibase ID: P54, Wikibase datatype: string    
### date    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/date    
**label_de** Datum    
**label_en** date    
**description_de** wird für Ereignisse verwendet, die mit einem Zeitpunkt und nicht mit einer Zeitspanne verbunden sind (verwenden Sie Start- und Enddatum zur Angabe einer Zeitspanne)    
**description_en** used for events that are associated with a point in time instead of a time span (use start date and end date for indicating a time span)    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** http://www.w3.org/2001/XMLSchema#date    
**equivalent property** http://www.wikidata.org/entity/P585    
**note** Wikibase ID: P12, Wikibase datatype: edtf    
### doi    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/doi    
**label_de** DOI    
**label_en** DOI    
**description_de** Seriencode zur eindeutigen Identifizierung digitaler Objekte wie wissenschaftlicher Arbeiten (nur Großbuchstaben verwenden)    
**description_en** serial code used to uniquely identify digital objects like academic papers (use upper case letters only)    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/digital_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P356    
**note** Wikibase ID: P103, Wikibase datatype: external-id    
### duration    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/duration    
**label_de** Dauer    
**label_en** duration    
**description_de** Dauer eines Objekts, in der Regel Video oder Audio    
**description_en** duration of an item, usually video or audio    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**subproperty of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_dimension    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P2047, https://schema.org/duration    
**note** Wikibase ID: P68, Wikibase datatype: quantity    
### end_date    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/end_date    
**label_de** Enddatum    
**label_en** end date    
**description_de** wenn ein Ereignis beendet wurde, auch die Eigenschaft »Startdatum« verwenden (die Eigenschaft »Datum« sollte alternativ verwendet werden, wenn ein Ereignis nur einen Zeitpunkt statt einer Zeitspanne hat)    
**description_en** when an event finished, use property 'start date' as well (the property 'date' should be used alternatively when an event only has a point in time instead of a time span)    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** http://www.w3.org/2001/XMLSchema#date    
**equivalent property** http://www.wikidata.org/entity/P582, https://schema.org/endDate    
**note** Wikibase ID: P11, Wikibase datatype: edtf    
### equipment    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/equipment    
**label_de** Ausrüstung    
**label_en** equipment    
**description_de** für die Erstellung von Medienobjekten verwendete Ausrüstung    
**description_en** equipment used for creation of media item    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** http://www.w3.org/2001/XMLSchema#string    
**note** Wikibase ID: P67, Wikibase datatype: string    
### external_link    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/external_link    
**label_de** externer Link    
**label_en** external link    
**description_de** verweist auf externe Ressourcen oder Mediendarstellungen    
**description_en** links to external resources or media representations    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** http://www.w3.org/2001/XMLSchema#string    
**note** Wikibase ID: P57, Wikibase datatype: url    
### file_extension    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/file_extension    
**label_de** Dateierweiterung    
**label_en** file extension    
**description_de** Dateierweiterung, die das Dateiformat angibt, das mit dem Klassen-Dateiformat verwendet wird    
**description_en** file extension that denotes the file format use with class file format    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/file_format    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P1195    
**note** Wikibase ID: P71, Wikibase datatype: string    
### file_size    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/file_size    
**label_de** Dateigröße    
**label_en** file size    
**description_de** Größe einer Medienelement-Datei    
**description_en** size of media item file    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**equivalent property** http://www.wikidata.org/entity/P3575    
**note** Wikibase ID: P69, Wikibase datatype: quantity    
### file_storage    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/file_storage    
**label_de** Dateispeicher    
**label_en** file storage    
**description_de** URL, unter der die Datei gespeichert ist    
**description_en** URL where file is stored    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P43_has_dimension    
**note** Wikibase ID: P73, Wikibase datatype: url    
### file_view    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/file_view    
**label_de** Dateiansicht    
**label_en** file view    
**description_de** URL, unter der die Datei eingesehen werden kann    
**description_en** URL where file can be viewed    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/media_item    
**range** http://www.w3.org/2001/XMLSchema#string    
**note** Wikibase ID: P74, Wikibase datatype: url    
### full_citation    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/full_citation    
**label_de** vollständige Quellenangabe    
**label_en** full citation    
**description_de** wie ein bibliographischer Titel zitiert werden soll    
**description_en** how a bibliographic item a would be cited    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/bibliographic_work    
**range** http://www.w3.org/2001/XMLSchema#string    
**note** Wikibase ID: P59, Wikibase datatype: string    
### geonames    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/geonames    
**label_de** Geonames    
**label_en** Geonames    
**description_de** Kennung in der GeoNames geographical database    
**description_en** identifier in the GeoNames geographical database    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/place    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P1566    
**note** Wikibase ID: P101, Wikibase datatype: external-id    
### getty_aat    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/getty_aat    
**label_de** Getty AAT    
**label_en** Getty AAT    
**description_de** Kennung im Art & Architecture Thesaurus des Getty Research Institute    
**description_en** identifier in the Art & Architecture Thesaurus by the Getty Research Institute    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P1014    
**note** Wikibase ID: P99, Wikibase datatype: external-id    
### getty_ulan    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/getty_ulan    
**label_de** Getty ULAN    
**label_en** Getty ULAN    
**description_de** Kennung aus der Union List of Artist Names des Getty Research Institute    
**description_en** identifier from the Union List of Artist Names by the Getty Research Institute    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P245    
**note** Wikibase ID: P100, Wikibase datatype: external-id    
### gnd    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/gnd    
**label_de** GND    
**label_en** GND    
**description_de** Kennung aus einer internationalen Normdatei für Personennamen, Körperschaften und Schlagwörter – Deutsche Nationalbibliothek    
**description_en** identifier from an international authority file of names, subjects, and organizations - Deutsche Nationalbibliothek    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/actor, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P227    
**note** Wikibase ID: P102, Wikibase datatype: external-id    
### has_dimension    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_dimension    
**label_de** hat Dimension    
**label_en** has dimension    
**description_de** übergeordnete Eigenschaft für Abmessungen wie Höhe und Breite    
**description_en** superproperty for dimensions like height and width    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P43_has_dimension    
**note** Wikibase ID: P31, Wikibase datatype: quantity    
### height    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/height    
**label_de** Höhe    
**label_en** height    
**description_de** Höhe eines Objekts    
**description_en** height of an item    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**subproperty of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_dimension    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#float    
**equivalent property** http://www.wikidata.org/entity/P2048, https://schema.org/height    
**note** Wikibase ID: P32, Wikibase datatype: quantity    
### iconclass    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/iconclass    
**label_de** Iconclass    
**label_en** Iconclass    
**description_de** Iconclass-Code, der einem künstlerischen Motiv oder Konzept entspricht    
**description_en** Iconclass code that corresponds with an artistic theme or concept    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/iconographic_concept    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P1256    
**note** Wikibase ID: P98, Wikibase datatype: external-id    
### image    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/image    
**label_de** Bild    
**label_en** image    
**description_de** visuelle Darstellung dieses Objekts, die in der Regel nur zur Dokumentation und nicht zur Darstellung verwendet wird (siehe auch P32)    
**description_en** visual depiction of this item, typically used for documentation only rather than representation (see also P32)    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/medial_item    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P18, https://schema.org/image    
**note** Wikibase ID: P61, Wikibase datatype: localMedia    
### inscription    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/inscription    
**label_de** Inschrift    
**label_en** inscription    
**description_de** Textstück auf einem Gegenstand    
**description_en** piece of text on an object    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P1684    
**note** Wikibase ID: P25, Wikibase datatype: string    
### internal_id    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/internal_id    
**label_de** interne ID    
**label_en** internal ID    
**description_de** interne Kennung für diesen Artikel    
**description_en** Internal identifier for this item    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**note** Wikibase ID: P96, Wikibase datatype: string    
### length    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/length    
**label_de** Länge    
**label_en** length    
**description_de** Länge eines Objekts    
**description_en** length of an item    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**subproperty of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_dimension    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#float    
**equivalent property** http://www.wikidata.org/entity/P2043    
**note** Wikibase ID: P33, Wikibase datatype: quantity    
### number_of_parts    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/number_of_parts    
**label_de** hat Anzahl Teile    
**label_en** number of parts    
**description_de** bei Objekten, die aus Teilen bestehen: wie viele Teile es gibt    
**description_en** for items that have parts: how many parts there are    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/human-made_object    
**range** http://www.w3.org/2001/XMLSchema#float    
**equivalent property** http://www.cidoc-crm.org/cidoc-crm/P57_has_number_of_parts    
**note** Wikibase ID: P7, Wikibase datatype: quantity    
### perspective_position_x-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/perspective_position_x-axis    
**label_de** Position im 3D Modell - Perspektive Position x-Achse    
**label_en** position in 3D model - perspective position x-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P84, Wikibase datatype: quantity    
### perspective_position_y-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/perspective_position_y-axis    
**label_de** Position im 3D Modell - Perspektive Position y-Achse    
**label_en** position in 3D model - perspective position y-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P85, Wikibase datatype: quantity    
### perspective_position_z-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/perspective_position_z-axis    
**label_de** Position im 3D Modell - Perspektive Position z-Achse    
**label_en** position in 3D model - perspective position z-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P86, Wikibase datatype: quantity    
### perspective_target_x-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/perspective_target_x-axis    
**label_de** Position im 3D Modell - Perspektive Ziel x-Achse    
**label_en** position in 3D model - perspective target x-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P87, Wikibase datatype: quantity    
### perspective_target_y-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/perspective_target_y-axis    
**label_de** Position im 3D Modell - Perspektive Ziel y-Achse    
**label_en** position in 3D model - perspective target y-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P88, Wikibase datatype: quantity    
### perspective_target_z-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/perspective_target_z-axis    
**label_de** Position im 3D Modell - Perspektive Ziel z-Achse    
**label_en** position in 3D model - perspective target z-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P89, Wikibase datatype: quantity    
### publication_date    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/publication_date    
**label_de** Veröffentlichungsdatum    
**label_en** publication date    
**description_de** Zeitpunkt, zu dem ein bibliografischer Titel veröffentlicht wurde    
**description_en** point in time when a bibliographic item was published    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/bibliographic_work    
**range** http://www.w3.org/2001/XMLSchema#date    
**equivalent property** http://www.wikidata.org/entity/P577, https://schema.org/datePublished    
**note** Wikibase ID: P60, Wikibase datatype: edtf    
### ranking    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/ranking    
**label_de** Rang    
**label_en** ranking    
**description_de** Rang, der die Reihenfolge der Anmerkungen im Kompakkt-Walkthrough bestimmt    
**description_en** rank determining the order of annotations in the Kompakkt walkthrough    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#integer    
**note** Wikibase ID: P79, Wikibase datatype: quantity    
### reference_url    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/reference_url    
**label_de** Referenz-URL    
**label_en** reference url    
**description_de** URL, in der eine Behauptung aufgestellt wurde – unter Stellungnahme im Feld »Referenzen« zu verwenden    
**description_en** URL where a claim was made - to be used in the references field under a statement    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P854    
**note** Wikibase ID: P56, Wikibase datatype: url    
### selector_ref_normal_x-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/selector_ref_normal_x-axis    
**label_de** Position im 3D Modell - Selektor Referenz Normale x-Achse    
**label_en** position in 3D model - selector ref normal x-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P93, Wikibase datatype: quantity    
### selector_ref_normal_y-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/selector_ref_normal_y-axis    
**label_de** Position im 3D Modell - Selektor Referenz Normale y-Achse    
**label_en** position in 3D model - selector ref normal y-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P94, Wikibase datatype: quantity    
### selector_ref_normal_z-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/selector_ref_normal_z-axis    
**label_de** Position im 3D Modell - Selektor Referenz Normale z-Achse    
**label_en** position in 3D model - selector ref normal z-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P95, Wikibase datatype: quantity    
### selector_ref_point_x-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/selector_ref_point_x-axis    
**label_de** Position im 3D Modell - Selektor Referenz Punkt x-Achse    
**label_en** position in 3D model - selector ref point x-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P90, Wikibase datatype: quantity    
### selector_ref_point_y-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/selector_ref_point_y-axis    
**label_de** Position im 3D Modell - Selektor Referenz Punkt y-Achse    
**label_en** position in 3D model - selector ref point y-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P91, Wikibase datatype: quantity    
### selector_ref_point_z-axis    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/selector_ref_point_z-axis    
**label_de** Position im 3D Modell - Selektor Referenz Punkt z-Achse    
**label_en** position in 3D model - selector ref point z-axis    
**description_de** Kompakkt-spezifische Eigenschaft der Annotationssposition - diese Eigenschaft wird nicht manuell ausgefüllt, sondern nur automatisch, wenn eine neue Annotation gespeichert wird    
**description_en** Kompakkt-specific annotation position property - this property is not meant to be filled out manually, only automatically when a new annotation is saved    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/annotation    
**range** http://www.w3.org/2001/XMLSchema#float    
**note** Wikibase ID: P92, Wikibase datatype: quantity    
### start_date    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/start_date    
**label_de** Startdatum    
**label_en** start date    
**description_de** wenn ein Ereignis begonnen hat, auch die Eigenschaft »Enddatum« verwenden (die Eigenschaft »Datum« sollte alternativ verwendet werden, wenn ein Ereignis nur einen Zeitpunkt statt einer Zeitspanne hat)    
**description_en** when an event started, use property 'end date' as well (the property 'date' should be used alternatively when an event only has a point in time instead of a time span)    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/event    
**range** http://www.w3.org/2001/XMLSchema#date    
**equivalent property** http://www.wikidata.org/entity/P580, https://schema.org/startDate    
**note** Wikibase ID: P10, Wikibase datatype: edtf    
### title    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/title    
**label_de** Titel    
**label_en** title    
**description_de** Titel eines kreativen Werks, wenn ausdrücklich angegeben    
**description_en** title of a creative work when explicitly given    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/conceptual_object, https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#string    
**equivalent property** http://www.wikidata.org/entity/P1476    
**note** Wikibase ID: P4, Wikibase datatype: string    
### weight    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/weight    
**label_de** Gewicht    
**label_en** weight    
**description_de** Gewicht eines Objekts    
**description_en** weight of an item    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**subproperty of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_dimension    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#float    
**equivalent property** http://www.wikidata.org/entity/P2067, https://schema.org/weight    
**note** Wikibase ID: P35, Wikibase datatype: quantity    
### width    
**iri** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/width    
**label_de** Breite    
**label_en** width    
**description_de** Breite eines Objekts    
**description_en** width of an item    
**type** http://www.w3.org/2002/07/owl#DatatypeProperty    
**subproperty of** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/has_dimension    
**domain** https://gitlab.com/nfdi4culture/ta1-data-enrichment/wikibase-model/physical_object    
**range** http://www.w3.org/2001/XMLSchema#float    
**equivalent property** http://www.wikidata.org/entity/P2049, https://schema.org/width    
**note** Wikibase ID: P34, Wikibase datatype: quantity    
